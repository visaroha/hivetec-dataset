class MenuPagesController < ApplicationController
  before_action :set_menu_page, only: %i[edit update destroy]

  # GET /menu_pages
  # GET /menu_pages.json
  def index
    @menu_pages = MenuPage.includes(:menu).all.page params[:page]
  end

  # GET /menu_pages/1
  # GET /menu_pages/1.json
  def show
    @menu_page = MenuPage.includes(menu_items: :dish).find(params[:id])
  end

  # GET /menu_pages/new
  def new
    @menu_page = MenuPage.new
  end

  # GET /menu_pages/1/edit
  def edit; end

  # POST /menu_pages
  # POST /menu_pages.json
  def create
    @menu_page = MenuPage.new(menu_page_params)

    respond_to do |format|
      if @menu_page.save
        format.html { redirect_to @menu_page, notice: 'Menu page was successfully created.' }
        format.json { render :show, status: :created, location: @menu_page }
      else
        format.html { render :new }
        format.json { render json: @menu_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /menu_pages/1
  # PATCH/PUT /menu_pages/1.json
  def update
    respond_to do |format|
      if @menu_page.update(menu_page_params)
        format.html { redirect_to @menu_page, notice: 'Menu page was successfully updated.' }
        format.json { render :show, status: :ok, location: @menu_page }
      else
        format.html { render :edit }
        format.json { render json: @menu_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /menu_pages/1
  # DELETE /menu_pages/1.json
  def destroy
    @menu_page.destroy
    respond_to do |format|
      format.html { redirect_to menu_pages_url, notice: 'Menu page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import
    MenuPage.bulk_import(params[:csv_file])
    flash[:success] = 'Menu Pages imported'
    redirect_to menu_pages_path
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_menu_page
    @menu_page = MenuPage.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def menu_page_params
    params.require(:menu).permit(:menu_id, :page_number, :image_id, :full_height, :full_width, :uuid)
  end
end
