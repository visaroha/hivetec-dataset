class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  rescue_from ActiveRecord::RecordNotFound do
    flash[:notice] = 'The object you tried to access does not exist'
    render file: "#{Rails.root}/public/404.html", status: 404
  end

  rescue_from ActionController::RoutingError do
    render file: "#{Rails.root}/public/404.html", status: 404
  end
end
