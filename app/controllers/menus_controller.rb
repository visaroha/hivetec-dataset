class MenusController < ApplicationController
  before_action :set_menu, only: %i[show edit update destroy]

  # GET /menus
  # GET /menus.json
  def index
    @q = Menu.ransack(params[:q])
    @menus = @q.result(distinct: true).page params[:page]

    respond_to do |format|
      format.html
      format.json do
        @menus = @menus.limit(false).offset(false) if params[:q] &&
                                                      params[:q][:export_all] == '1'
        render json: @menus
      end
    end
  end

  # GET /menus/1
  # GET /menus/1.json
  def show
    @menu_pages = @menu.menu_pages.includes(menu_items: :dish).
                  order('menu_pages.page_number')
  end

  # GET /menus/new
  def new
    @menu = Menu.new
  end

  # GET /menus/1/edit
  def edit; end

  # POST /menus
  # POST /menus.json
  def create
    @menu = Menu.new(menu_params)

    respond_to do |format|
      if @menu.save
        format.html { redirect_to @menu, notice: 'Menu was successfully created.' }
        format.json { render :show, status: :created, location: @menu }
      else
        format.html { render :new }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /menus/1
  # PATCH/PUT /menus/1.json
  def update
    respond_to do |format|
      if @menu.update(menu_params)
        format.html { redirect_to @menu, notice: 'Menu was successfully updated.' }
        format.json { render :show, status: :ok, location: @menu }
      else
        format.html { render :edit }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /menus/1
  # DELETE /menus/1.json
  def destroy
    @menu.destroy
    respond_to do |format|
      format.html { redirect_to menus_url, notice: 'Menu was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import
    Menu.bulk_import(params[:csv_file])
    flash[:success] = 'Menus imported'
    redirect_to menus_path
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_menu
    @menu = Menu.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def menu_params
    params.require(:menu).permit(:name, :sponsor, :event, :venue, :place,
                                 :physical_description, :occasion, :notes,
                                  :call_number, :keywords, :language, :date,
                                 :location, :location_type, :currency, :currency_symbol,
                                  :status, :page_count, :dish_count)
  end
end
