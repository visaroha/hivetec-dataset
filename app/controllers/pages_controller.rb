class PagesController < ApplicationController
  def viewtype
    params[:type] = 'place' unless %w[place venue event].include? params[:type]
    @objs = Menu.all.group(params[:type].to_sym).order('count_id desc').count('id')
  end
end
