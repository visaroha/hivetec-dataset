json.extract! menu, :id, :name, :sponsor, :event, :venue, :place, :physical_description, :occasion, :notes, :call_number, :keywords, :language, :date, :location, :location_type, :currency, :currency_symbol, :status, :page_count, :dish_count, :created_at, :updated_at
json.url menu_url(menu, format: :json)
