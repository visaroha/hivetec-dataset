json.extract! menu_item, :id, :menu_page_id, :price, :high_price, :dish_id, :xpos, :ypos, :created_at, :updated_at
json.url menu_item_url(menu_item, format: :json)
