json.extract! dish, :id, :name, :description, :first_appeared, :last_appeared, :lowest_price, :highest_price, :created_at, :updated_at
json.url dish_url(dish, format: :json)
