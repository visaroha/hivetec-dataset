json.extract! menu_page, :id, :created_at, :updated_at
json.url menu_page_url(menu_page, format: :json)
