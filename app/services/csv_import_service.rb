# Class to import data from CSV files
require 'csv'

class CsvImportService
  def initialize(type, file)
    @type = type
    @file = file
  end

  def process
    return unless %w[menu menu_item menu_page dish].include? @type.name.underscore
    objs = []
    current_count = 0

    CSV.foreach(@file.path, headers: true, encoding: 'ISO-8859-1') do |row|
      objs << @type.new(row.to_h)
      current_count += 1
      if current_count > 1_000 # limit transcation size
        current_count = 0
        @type.import(objs)
        objs = []
      end
    end
    @type.import(objs) unless objs.empty?
  end
end
