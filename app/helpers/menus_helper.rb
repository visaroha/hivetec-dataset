module MenusHelper
  def menu_search_params
    resp = {}
    if params['q'].present?
      params['q'].each do |k, v|
        resp[k] = v
      end
      return resp
    end
  end
end
