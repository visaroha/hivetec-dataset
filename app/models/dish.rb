# Contains Dish class
# == Schema Information
#
# Table name: dishes
#
#  id             :integer          not null, primary key
#  name           :string
#  description    :string
#  first_appeared :integer
#  last_appeared  :integer
#  lowest_price   :float
#  highest_price  :float
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  menus_appeared :integer
#  times_appeared :integer
#

class Dish < ApplicationRecord
  validates :name, presence: true

  has_many :menu_items, inverse_of: :Dish

  def self.bulk_import(file)
    CsvImportService.new(self, file).process
  end
end
