# == Schema Information
#
# Table name: menus
#
#  id                   :integer          not null, primary key
#  name                 :string
#  sponsor              :string
#  event                :string
#  venue                :string
#  place                :string
#  physical_description :string
#  occasion             :string
#  notes                :text
#  call_number          :string
#  keywords             :string
#  language             :string
#  date                 :date
#  location             :string
#  location_type        :string
#  currency             :string
#  currency_symbol      :string
#  status               :string
#  page_count           :string
#  dish_count           :string
#

class Menu < ApplicationRecord
  has_many :menu_pages, inverse_of: :menu

  attr_reader :export_all

  def self.bulk_import(file)
    CsvImportService.new(self, file).process
  end
end
