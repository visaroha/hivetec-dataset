# == Schema Information
#
# Table name: menu_items
#
#  id           :integer          not null, primary key
#  menu_page_id :integer
#  price        :float
#  high_price   :float
#  dish_id      :integer
#  xpos         :decimal(, )      not null
#  ypos         :decimal(, )      not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class MenuItem < ApplicationRecord
  belongs_to :menu_page
  belongs_to :dish
  has_one :menu, through: :menu_page

  validates_presence_of :xpos, :ypos
  validates_associated :menu_page
  validates_associated :dish

  def self.bulk_import(file)
    CsvImportService.new(self, file).process
  end
end
