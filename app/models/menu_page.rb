# == Schema Information
#
# Table name: menu_pages
#
#  id          :integer          not null, primary key
#  menu_id     :integer          not null
#  page_number :integer
#  image_id    :integer          not null
#  full_height :integer
#  full_width  :integer
#  uuid        :string           not null
#

class MenuPage < ApplicationRecord
  belongs_to :menu
  has_many :menu_items, inverse_of: :menu_page

  validates :menu_id, uniqueness: { scope: :page_number,
                                    message: 'Duplicate entry for menu/page number combination' }

  validates_presence_of :menu_id, :image_id, :uuid

  # uuid is assumed to not be unique as the data provided has many duplicates

  def self.bulk_import(file)
    CsvImportService.new(self, file).process
  end
end
