# == Schema Information
#
# Table name: dishes
#
#  id             :integer          not null, primary key
#  name           :string
#  description    :string
#  first_appeared :integer
#  last_appeared  :integer
#  lowest_price   :float
#  highest_price  :float
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  menus_appeared :integer
#  times_appeared :integer
#

require 'rails_helper'
require 'csv'

RSpec.describe Dish, type: :model do
  it 'is valid with valid attributes' do
    dish = Dish.new(name: 'test dish', description: 'a test dish',
                    first_appeared: 1981, last_appeared: 1999, lowest_price: 0.1,
                    highest_price: 0.33, menus_appeared: 10, times_appeared: 12)
    expect(dish).to be_valid
  end

  it 'is not valid without a name' do
    dish = Dish.new(name: nil, description: 'a test dish',
                    first_appeared: 1981, last_appeared: 1999, lowest_price: 0.1,
                    highest_price: 0.33, menus_appeared: 10, times_appeared: 12)
    expect(dish).not_to be_valid
  end

  # it "can import dishes data from CSV" do
  #   csv_str = CSV.generate('data.csv', 'wb') do |csv|
  #     csv << ["name"] #hash keys
  #     csv << ["Test dish import 1"]
  #     csv << ["Test dish import 2"]
  #     csv << ["Test dish import 3"]
  #   end
  #   file = Tempfile.open(csv_str)

  #   Dish.bulk_import(file)
  #   expect(Dish.find_by(name: 'Test dish import 1')).not_to be_nil
  #   expect(Dish.find_by(name: 'Test dish import 2')).not_to be_nil
  # end
end
