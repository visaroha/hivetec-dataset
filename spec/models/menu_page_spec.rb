# == Schema Information
#
# Table name: menu_pages
#
#  id          :integer          not null, primary key
#  menu_id     :integer          not null
#  page_number :integer
#  image_id    :integer          not null
#  full_height :integer
#  full_width  :integer
#  uuid        :string           not null
#

require 'rails_helper'

RSpec.describe MenuPage, type: :model do
  describe 'Validations' do
    let(:menu) do
      Menu.create(name: 'test', place: 'testp', venue: 'testv', event: 'teste')
    end

    it 'is valid with valid attributes' do
      obj = described_class.new(menu_id: menu.id, page_number: 2,
                                image_id: 11_001, uuid: 'aasdf-1123-aasdrtg')
      expect(obj).to be_valid
    end

    it 'is no valid without menu_id' do
      obj = described_class.new(menu_id: nil, page_number: 2,
                                image_id: 11_001, uuid: 'aasdf-1123-aasdrtg')
      expect(obj).not_to be_valid
    end

    it 'is not valid without image_id' do
      obj = described_class.new(menu_id: menu.id, page_number: 2,
                                image_id: nil, uuid: 'aasdf-1123-aasdrtg')
      expect(obj).not_to be_valid
    end

    it 'is not valid without uuid' do
      obj = described_class.new(menu_id: menu.id, page_number: 2,
                                image_id: 11_001, uuid: nil)
      expect(obj).not_to be_valid
    end

    it 'is not valid if menu_id and page is not unique' do
      obj1 = described_class.new(menu_id: menu.id, page_number: 2,
                                 image_id: 11_001, uuid: 'aasdf-1123-aasdrtg')
      described_class.create(menu_id: menu.id, page_number: 2,
                             image_id: 11_001, uuid: 'aasdf-1123-aasdrtg')
      expect(obj1).not_to be_valid
    end
  end
end
