# == Schema Information
#
# Table name: menu_items
#
#  id           :integer          not null, primary key
#  menu_page_id :integer
#  price        :float
#  high_price   :float
#  dish_id      :integer
#  xpos         :decimal(, )      not null
#  ypos         :decimal(, )      not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe MenuItem, type: :model do
  describe 'Validations' do
    let(:menu) do
      Menu.create(name: 'test', place: 'testp', venue: 'testv', event: 'teste')
    end
    let(:menu_page) do
      MenuPage.create(menu_id: menu.id, page_number: 2,
        image_id: 11_001, uuid: 'aasdf-1123-aasdrtg')
    end
    let(:dish) do
      Dish.create(name: 'test dish', description: 'a test dish',
        first_appeared: 1981, last_appeared: 1999, lowest_price: 0.1,
        highest_price: 0.33, menus_appeared: 10, times_appeared: 12)
    end

    it 'is valid with valid attributes' do
      obj = described_class.new(menu_page_id: menu_page.id, price: 1.56,
        high_price: 2.09, dish_id: dish.id, xpos: 0.5453, ypos: 0.0999991)
      expect(obj).to be_valid
    end

    it 'is not valid without xpos' do
      obj = described_class.new(menu_page_id: menu_page.id, price: 1.56,
        high_price: 2.09, dish_id: dish.id, xpos: nil, ypos: 0.0999991)
      expect(obj).not_to be_valid
    end

    it 'is not valid without ypos' do
      obj = described_class.new(menu_page_id: menu_page.id, price: 1.56,
        high_price: 2.09, dish_id: dish.id, xpos: 1.00, ypos: nil)
      expect(obj).not_to be_valid
    end

    it 'is not valid without menu_page_id' do
      obj = described_class.new(menu_page_id: nil, price: 1.56,
        high_price: 2.09, dish_id: dish.id, xpos: 0.5453, ypos: 0.991)
      expect(obj).not_to be_valid
    end

    it 'is not valid without valid Menu Page' do
      obj = described_class.new(menu_page_id: 12_345, price: 1.56,
        high_price: 2.09, dish_id: dish.id, xpos: 0.5453, ypos: 0.991)
      expect(obj).not_to be_valid
    end
  end
end
