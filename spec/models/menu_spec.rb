# == Schema Information
#
# Table name: menus
#
#  id                   :integer          not null, primary key
#  name                 :string
#  sponsor              :string
#  event                :string
#  venue                :string
#  place                :string
#  physical_description :string
#  occasion             :string
#  notes                :text
#  call_number          :string
#  keywords             :string
#  language             :string
#  date                 :date
#  location             :string
#  location_type        :string
#  currency             :string
#  currency_symbol      :string
#  status               :string
#  page_count           :string
#  dish_count           :string
#

require 'rails_helper'

RSpec.describe Menu, type: :model do
  describe 'Validations' do
    it 'is valid with valid attributes' do
      menu = described_class.new(name: 'test menu', physical_description: 'a test menu',
                                 date: '2003-04-11', location: 'MAD', event: 'ZZXAA',
                                 venue: 'BANQUET HALL', call_number: '200102-292', place: 'NSQ')
      expect(menu).to be_valid
    end
  end
end
