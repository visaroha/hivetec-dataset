Rails.application.routes.draw do
  get 'pages/viewtype', as: 'page', alias: 'p'

  resources :menu_items
  resources :menus do
    collection { post 'import' }
  end

  resources :menu_pages do
    collection { post 'import' }
  end

  resources :dishes do
    collection { post 'import' }
  end

  resources :menu_items do
    collection { post 'import' }
  end

  root to: 'menus#index'
end
