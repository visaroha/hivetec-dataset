# About

- Ruby on rails implementation for - https://github.com/igas/hivetec-code

- Live demo (with all data loaded) is available here - https://hivetec.herokuapp.com/
