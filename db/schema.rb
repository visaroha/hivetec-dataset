# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171201035011) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "dishes", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.integer "first_appeared"
    t.integer "last_appeared"
    t.float "lowest_price"
    t.float "highest_price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "menus_appeared"
    t.integer "times_appeared"
  end

  create_table "menu_items", force: :cascade do |t|
    t.bigint "menu_page_id"
    t.float "price"
    t.float "high_price"
    t.bigint "dish_id"
    t.decimal "xpos", null: false
    t.decimal "ypos", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["dish_id"], name: "index_menu_items_on_dish_id"
    t.index ["menu_page_id"], name: "index_menu_items_on_menu_page_id"
  end

  create_table "menu_pages", force: :cascade do |t|
    t.bigint "menu_id", null: false
    t.integer "page_number"
    t.bigint "image_id", null: false
    t.integer "full_height"
    t.integer "full_width"
    t.string "uuid", null: false
    t.index ["menu_id"], name: "index_menu_pages_on_menu_id"
  end

  create_table "menus", force: :cascade do |t|
    t.string "name"
    t.string "sponsor"
    t.string "event"
    t.string "venue"
    t.string "place"
    t.string "physical_description"
    t.string "occasion"
    t.text "notes"
    t.string "call_number"
    t.string "keywords"
    t.string "language"
    t.date "date"
    t.string "location"
    t.string "location_type"
    t.string "currency"
    t.string "currency_symbol"
    t.string "status"
    t.string "page_count"
    t.string "dish_count"
  end

  add_foreign_key "menu_items", "dishes"
  add_foreign_key "menu_items", "menu_pages"
  add_foreign_key "menu_pages", "menus"
end
