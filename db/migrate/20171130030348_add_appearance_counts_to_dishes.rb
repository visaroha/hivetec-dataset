class AddAppearanceCountsToDishes < ActiveRecord::Migration[5.1]
  def change
    add_column :dishes, :menus_appeared, :integer
    add_column :dishes, :times_appeared, :integer
  end
end
