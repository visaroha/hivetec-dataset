class CreateMenuPages < ActiveRecord::Migration[5.1]
  def change
    create_table :menu_pages do |t|
      t.belongs_to :menu, foreign_key: true, null: false
      t.integer :page_number
      t.bigint :image_id, null: false
      t.integer :full_height
      t.integer :full_width
      t.string :uuid, null: false
    end
  end
end
