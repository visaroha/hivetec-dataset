class CreateMenuItems < ActiveRecord::Migration[5.1]
  def change
    create_table :menu_items do |t|
      t.belongs_to :menu_page, foreign_key: true
      t.float :price
      t.float :high_price
      t.belongs_to :dish, foreign_key: true
      t.decimal :xpos, null: false
      t.decimal :ypos, null: false

      t.timestamps
    end
  end
end
